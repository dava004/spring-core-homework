package com.epam.springhomework.marvelheroes.factories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.FactoryBean;

import com.epam.springhomework.marvelheroes.model.Sentinel;
import com.epam.springhomework.marvelheroes.model.Skill;

public class SentinelFactory implements FactoryBean<Sentinel> {

	private static int cnt = 0;
	private int health;
	private List<Skill> skills; 

	public int getHealth() {
		return health;
	}

	public SentinelFactory(int health) {
		this.health = health;
	}

	public Sentinel getObject() throws Exception {
		Sentinel sentinel = new Sentinel();
		sentinel.setHealth(health);
		sentinel.setSkills(skills);
		sentinel.setName("Sentinel" + cnt);
		cnt++;
		return sentinel;
	}

	protected void createOneElementSkillList() {
		Skill skill = new Skill("sentinel weapon", 100, 0);
		skills = new ArrayList<Skill>();
		skills.add(skill);
	}

	public Class<?> getObjectType() {
		return Sentinel.class;
	}

	public boolean isSingleton() {
		return false;
	}
}
