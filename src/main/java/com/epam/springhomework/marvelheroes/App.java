package com.epam.springhomework.marvelheroes;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.springhomework.marvelheroes.model.War;

public class App {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		final ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		
		Map<String, War> wars = context.getBeansOfType(War.class);
		for(Map.Entry<String, War> e : wars.entrySet()) {
			e.getValue().letTheWarBegins();
		}
	}
}
