package com.epam.springhomework.marvelheroes.model;

public enum Winner {
	HEROES, VILLAINS, IN_PROGRESS, NONE
}
