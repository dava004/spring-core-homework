package com.epam.springhomework.marvelheroes.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Character {

	private String name;
	private String alterEgoName;
	private List<Skill> skills = new ArrayList<Skill>();
	private boolean alive;
	private int health;

	public Character() {
		alive = true;
	}

	public void attackEnemy(Character enemy) {
		Skill firstAttackSkill = null;
		for (Skill skill : skills) {
			if (skill.isAttackSkill()) {
				firstAttackSkill = skill;
				break;
			}
		}

		if (null != firstAttackSkill) {
			enemy.protectYourself(firstAttackSkill.getAttackValue());
		}
	}

	private void protectYourself(int totalHit) {
		Skill protectionSkill = null;
		for (Skill skill : skills) {
			if (skill.isProtectionSkill()) {
				protectionSkill = skill;
				break;
			}
		}
		int protectionValue = 0;
		if (null != protectionSkill) {
			protectionValue = protectionSkill.getProtectionValue();
		}
		if (totalHit > protectionValue) {
			totalHit -= protectionValue;
			health -= totalHit;
			if (health <= 0) {
				health = 0;
				alive = false;
			}
		}
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public String getAlterEgoName() {
		return alterEgoName;
	}

	public void setAlterEgoName(String alterEgoName) {
		this.alterEgoName = alterEgoName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Skill> getSkills() {
		return Collections.unmodifiableList(skills);
	}

	public void setSkills(List<Skill> skills) {
		if (null != skills) {
			this.skills.clear();
			this.skills.addAll(skills);
		}
	}

	@Override
	public String toString() {
		return name + " " + "(" + alterEgoName + ")" + "(" + health + ")" + skills;
	}
}
