package com.epam.springhomework.marvelheroes.model;

public class Location {

	private Integer earthNumber;
	private String earthDescription;

	public Location(Integer earthNumber, String earthDescription) {
		super();
		this.earthNumber = earthNumber;
		this.earthDescription = earthDescription;
	}

	@Override
	public String toString() {
		return "Earth-" + earthNumber;
	}

	public Integer getEarthNumber() {
		return earthNumber;
	}

	public void setEarthNumber(Integer earthNumber) {
		this.earthNumber = earthNumber;
	}

	public String getEarthDescription() {
		return earthDescription;
	}

	public void setEarthDescription(String earthDescription) {
		this.earthDescription = earthDescription;
	}

}
