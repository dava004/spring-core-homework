package com.epam.springhomework.marvelheroes.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class War {
	private final List<Character> villains = new ArrayList<Character>();
	private final List<Character> heroes = new ArrayList<Character>();
	protected Location location;
	private static final Logger logger = LoggerFactory.getLogger(War.class);

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<Character> getHeroes() {
		return heroes;
	}

	public void setHeroes(List<? extends Character> heroes) {
		if (null != this.heroes) {
			this.heroes.clear();
		}
		this.heroes.addAll(heroes);
	}

	public List<Character> getVillains() {
		return villains;
	}

	public void setVillains(List<? extends Character> villains) {
		if (null != this.villains) {
			this.villains.clear();
		}
		this.villains.addAll(villains);
	}

	// @Override
	// public String toString() {
	// return "War happens on " + location
	// + System.getProperty("line.separator") + "heroes=" + heroes
	// + System.getProperty("line.separator") + "villains=" + villains;
	// }

	public void letTheWarBegins() {
		logger.info("War begins on" + location);

		Winner winner = Winner.IN_PROGRESS;

		while (winner != Winner.HEROES && winner != Winner.VILLAINS
				&& winner != Winner.NONE) {

			villainsTurn();
			heroesTurn();

			if (heroes.isEmpty() && villains.isEmpty()) {
				winner = Winner.NONE;
			} else if (heroes.isEmpty()) {
				winner = Winner.VILLAINS;
			} else if (villains.isEmpty()) {
				winner = Winner.HEROES;
			} else {
				winner = Winner.IN_PROGRESS;
			}
		}
		if (Winner.HEROES == winner) {
			logger.info("Heroes won!");
		} else if (Winner.VILLAINS == winner) {
			logger.info("Villains won! :(");
		} else {
			logger.error("There are no winners! (this should not happen)");
		}

		logger.info("War on " + location + "is finished.");
	}

	private void heroesTurn() {
		Random random = new Random();
		for (Character hero : heroes) {
			if (hero.isAlive() && !villains.isEmpty()) {
				Character rndVillain = villains.get(random.nextInt(villains
						.size()));
				hero.attackEnemy(rndVillain);
				logger.info(hero.getName() + " attacks " + rndVillain.getName());
				if (!rndVillain.isAlive()) {
					villains.remove(rndVillain);
					logger.info(rndVillain.getName() + " is dead.");
				}
			}
		}
	}

	private void villainsTurn() {
		Random random = new Random();
		for (Character villain : villains) {
			if (villain.isAlive() && !heroes.isEmpty()) {
				Character rndHero = heroes.get(random.nextInt(heroes.size()));
				villain.attackEnemy(rndHero);
				logger.info(villain.getName() + " attacks " + rndHero.getName());

				if (!rndHero.isAlive()) {
					heroes.remove(rndHero);
					logger.info(rndHero.getName() + " is dead.");
				}
			}
		}
	}
}
