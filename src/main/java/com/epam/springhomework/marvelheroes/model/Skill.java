package com.epam.springhomework.marvelheroes.model;

public class Skill {
	
	private String name;
	private int attackValue;
	private int protectionValue;
	
	public Skill(String name, int attackValue, int protectionValue) {
		this.name =name;
		this.attackValue = attackValue;
		this.protectionValue = protectionValue;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAttackValue() {
		return attackValue;
	}

	public void setAttackValue(int attackValue) {
		this.attackValue = attackValue;
	}

	public int getProtectionValue() {
		return protectionValue;
	}

	public void setProtectionValue(int protectionValue) {
		this.protectionValue = protectionValue;
	}
	
	public boolean isAttackSkill() {
		return attackValue>0;
	}
	
	public boolean isProtectionSkill() {
		return protectionValue>0;
	}

	@Override
	public String toString() {
		return "Skill [name=" + name + ", attackValue=" + attackValue
				+ ", protectionValue=" + protectionValue + "]";
	}
	
	
}
